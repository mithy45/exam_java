package fr.epita.UserService.service;

import java.util.List;

import fr.epita.UserService.entity.User;

public interface IUserService {
    List<User> getAllUsers();
}
