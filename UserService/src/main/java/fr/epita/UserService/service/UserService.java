package fr.epita.UserService.service;

import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Service;

import fr.epita.UserService.entity.User;

@Service
public class UserService implements IUserService {

    @Override
    public List<User> getAllUsers() {
        return Collections.singletonList(new User());
    }
    
}
